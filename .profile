# Automatically gets sourced by shell on boot

export SHELL="/bin/zsh"
export __GL_THREADED_OPTIMIZATION=1
export __GL_SHADER_DISK_CACHE=1
export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/home/cassidy/.local/share/flatpak/exports/bin:/var/lib/flatpak/exports/bin:/opt/intel/mediasdk/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/home/cassidy/.local/bin"
