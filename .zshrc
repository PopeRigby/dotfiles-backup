# History
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

# Use Vi as editor
bindkey -v

zstyle :compinstall filename '/home/cassidy/.zshrc'

# Command completion
autoload -Uz compinit
compinit

# Git integration
source /home/cassidy/.gitstatus/gitstatus.prompt.zsh

# Prompt
autoload -Uz promptinit
promptinit
autoload -U colors
colors
PROMPT='%F{blue}%~%f ${GITSTATUS_PROMPT}${GITSTATUS_PROMPT:+ }%(!.%F{red}#%f .%F{yellow}$%f '

# Dotfiles backup
alias conf='/usr/bin/git --git-dir=/home/cassidy/.myconfig/ --work-tree=/home/cassidy'

# Automatic rehash
zshcache_time="$(date +%s%N)"
autoload -Uz add-zsh-hook
rehash_precmd() {
  if [[ -a /var/cache/zsh/pacman ]]; then
    local paccache_time="$(date -r /var/cache/zsh/pacman +%s%N)"
    if (( zshcache_time < paccache_time )); then
      rehash
      zshcache_time="$paccache_time"
    fi
  fi
}
add-zsh-hook -Uz precmd rehash_precmd

# Help
autoload -Uz run-help
autoload -Uz run-help-git
autoload -Uz run-help-ip
autoload -Uz run-help-openssl
autoload -Uz run-help-p4
autoload -Uz run-help-sudo
autoload -Uz run-help-svk
autoload -Uz run-help-svn

# Fish-like syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# "Command not found" handler
source /usr/share/doc/pkgfile/command-not-found.zsh

# Pull aliases from .aliases
source /home/cassidy/.aliases

# Coloring
alias ls='ls --color=auto'
alias grep='grep --color=auto'
man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}
ZSH_HIGHLIGHT_STYLES[path]='fg=blue'
